using UnityEngine;

public class Area : MonoBehaviour
{
    [SerializeField] private Vector2 size = Vector2.one * 10f;

    public Vector3 GetRandomPosition()
    {
        var x = Random.Range(-size.x / 2f, size.x / 2f);
        var z = Random.Range(-size.y / 2f, size.y / 2f);
        
        return transform.position + new Vector3(x, 0f, z);
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(size.x, 0f, size.y));
    }
}
