﻿public interface ILightEmitter
{
    public void TurnOnLights();
    public void TurnOffLights();
}