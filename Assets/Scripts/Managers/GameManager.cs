using System.Collections.Generic;
using System.Linq;
using GameObjects;
using TMPro;
using UnityEngine;
using Plane = GameObjects.Plane;
using Random = UnityEngine.Random;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        [Header("Planes & Hangars")]
        [SerializeField] private List<Plane> planes;
        [SerializeField] private List<Hangar> hangars;

        [Header("Other")]
        [SerializeField] private TextMeshPro allPlanesLandedText;

        private int _parkedPlanesCount;
        private FloodLight[] _floodLights;

        private void Awake()
        {
            _floodLights = FindObjectsOfType<FloodLight>();
        }

        private void Start()
        {
            allPlanesLandedText.gameObject.SetActive(false);

            foreach (var plane in planes)
            {
                plane.onParked.AddListener(OnPlaneParked);
                plane.onMove.AddListener(OnPlaneMove);
            }

            AssignRandomNumberToHangars();
            AssignHangarsToPlanes();
        }

        private void OnPlaneParked()
        {
            _parkedPlanesCount++;

            if (_parkedPlanesCount == planes.Count)
            {
                allPlanesLandedText.gameObject.SetActive(true);
            }
        }

        private void OnPlaneMove()
        {
            if (_parkedPlanesCount <= 0)
            {
                return;
            }

            _parkedPlanesCount--;

            if (_parkedPlanesCount < planes.Count)
            {
                allPlanesLandedText.gameObject.SetActive(false);
            }
        }

        private void AssignRandomNumberToHangars()
        {
            var randomNumbers = GetRandomUniqueNumbers(hangars.Count);

            for (var i = 0; i < hangars.Count; i++)
            {
                hangars[i].Number = randomNumbers[i];
            }
        }

        private void AssignHangarsToPlanes()
        {
            var shuffledPlanes = planes.OrderBy(_ => Random.value).ToList();

            for (var i = 0; i < shuffledPlanes.Count; i++)
            {
                var plane = shuffledPlanes[i];

                plane.AssignedHangar = hangars[i];
            }
        }

        private static List<int> GetRandomUniqueNumbers(int count)
        {
            var numbers = new List<int>();
        
            while (numbers.Count < count)
            {
                var number = Random.Range(0, count);
            
                if (!numbers.Contains(number))
                {
                    numbers.Add(number);
                }
            }

            return numbers;
        }

        #region Button Events

        public void TurnOnAllPlaneLights()
        {
            foreach (var plane in planes)
            {
                plane.TurnOnLights();
            }
        }

        public void TurnOffAllPlaneLights()
        {
            foreach (var plane in planes)
            {
                plane.TurnOffLights();
            }
        }

        public void TurnOnAllFloodLights()
        {
            foreach (var floodLight in _floodLights)
            {
                floodLight.TurnOnLights();
            }
        }

        public void TurnOffAllFloodLights()
        {
            foreach (var floodLight in _floodLights)
            {
                floodLight.TurnOffLights();
            }
        }

        public void ParkAllPlanes()
        {
            foreach (var plane in planes)
            {
                plane.Park();
            }
        }

        public void RoamAllPlanes()
        {
            foreach (var plane in planes)
            {
                plane.StartRoaming();
            }
        }

        #endregion
    }
}
