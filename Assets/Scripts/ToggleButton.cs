using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ToggleButton : MonoBehaviour
{
    [SerializeField] private string onText;
    [SerializeField] private UnityEvent onToggleOn;
    
    [SerializeField] private string offText;
    [SerializeField] private UnityEvent onToggleOff;

    private bool _isToggled;
    private Button _button;
    private TextMeshProUGUI _buttonText;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _buttonText = _button.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        _button.onClick.AddListener(Toggle);

        _buttonText.text = onText;
    }

    private void Toggle()
    {
        _isToggled = !_isToggled;
        
        if (_isToggled)
        {
            onToggleOn?.Invoke();
            _buttonText.text = offText;
        }
        else
        {
            onToggleOff?.Invoke();
            _buttonText.text = onText;
        }
    }
}
