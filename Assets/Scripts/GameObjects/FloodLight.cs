﻿using UnityEngine;

namespace GameObjects
{
    public class FloodLight : MonoBehaviour, ILightEmitter
    {
        private Light[] _lights;

        private void Awake()
        {
            _lights = GetComponentsInChildren<Light>();
        }

        private void Start()
        {
            TurnOffLights();
        }

        public void TurnOnLights()
        {
            foreach (var lightSource in _lights)
            {
                lightSource.enabled = true;
            }
        }

        public void TurnOffLights()
        {
            foreach (var lightSource in _lights)
            {
                lightSource.enabled = false;
            }
        }
    }
}
