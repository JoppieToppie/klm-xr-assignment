using TMPro;
using UnityEngine;

namespace GameObjects
{
    public class Hangar : MonoBehaviour
    {
        [SerializeField] private Transform parkSpot;
        [SerializeField] private TextMeshPro text;

        [SerializeField] private Color defaultColor;
        [SerializeField] private Color occupiedColor;

        private int _number;

        public int Number
        {
            get => _number;
            set
            {
                _number = value;
                
                text.text = $"Hangar\n{_number}";
            }
        }

        public bool IsOccupied
        {
            set => text.color = value ? occupiedColor : defaultColor;
        }

        public Vector3 ParkSpot => parkSpot.position;

        private void Start()
        {
            text.color = defaultColor;
        }
    }
}
