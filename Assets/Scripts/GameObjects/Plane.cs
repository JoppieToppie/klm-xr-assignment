using System.Collections;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace GameObjects
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Plane : MonoBehaviour, ILightEmitter
    {
        [SerializeField] private TextMeshPro text;
        [SerializeField] private PlaneSO settings;
        [SerializeField] private Area roamArea;
        
        [Header("Roaming")]
        [SerializeField] private Vector2 roamDelayRange;

        [Header("Events")]
        public UnityEvent onMove;
        public UnityEvent onStop;
        public UnityEvent onParked;

        // todo: use stage machine?
        private IEnumerator _roamCoroutine;
        private NavMeshAgent _agent;
        private bool _hasReachedDestination;
        private bool _isParked;
        private bool _isNavigatingToHangar;

        private Hangar _assignedHangar;
        private Light[] _lights;

        public Hangar AssignedHangar
        {
            get => _assignedHangar;
            set
            {
                _assignedHangar = value;

                UpdateText();
            }
        }

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _lights = GetComponentsInChildren<Light>();
        }

        private void Start()
        {
            TurnOffLights();

            _agent.speed = settings.MoveSpeed;

            StartRoaming();
        }

        private void Update()
        {
            if (!_hasReachedDestination && _agent.remainingDistance <= _agent.stoppingDistance)
            {
                _hasReachedDestination = true;

                if (_isNavigatingToHangar)
                {
                    ReachedHanger();
                }
                else
                {
                    ReachedRoamDestination();
                }
            }
        }

        private void ReachedHanger()
        {
            _isParked = true;

            _assignedHangar.IsOccupied = true;

            onStop.Invoke();
            onParked.Invoke();
        }

        private void ReachedRoamDestination()
        {
            onStop.Invoke();
        }

        public void StartRoaming()
        {
            if (_isParked)
            {
                _assignedHangar.IsOccupied = false;
            }

            _hasReachedDestination = false;
            _isNavigatingToHangar = false;
            _isParked = false;

            _roamCoroutine = Roam();
            StartCoroutine(_roamCoroutine);
        }

        public void Park()
        {
            // if we are already parked in a hangar, don't do anything
            if (_isParked)
            {
                return;
            }

            StopCoroutine(_roamCoroutine);

            _agent.SetDestination(AssignedHangar.ParkSpot);
            _isNavigatingToHangar = true;
            _hasReachedDestination = false;
            onMove.Invoke();
        }

        private void UpdateText()
        {
            text.text = $"{settings.Brand}\n{settings.Type}\n{_assignedHangar.Number}";
        }

        private IEnumerator Roam()
        {
            while (true)
            {
                var randomPosition = roamArea.GetRandomPosition();

                _agent.SetDestination(randomPosition);
                _hasReachedDestination = false;
                onMove.Invoke();

                yield return new WaitForSeconds(Random.Range(roamDelayRange.x, roamDelayRange.y));
            }
        }

        public void TurnOnLights()
        {
            foreach (var lightSource in _lights)
            {
                lightSource.enabled = true;
            }
        }

        public void TurnOffLights()
        {
            foreach (var lightSource in _lights)
            {
                lightSource.enabled = false;
            }
        }
    }
}
