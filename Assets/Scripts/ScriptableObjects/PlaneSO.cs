using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlaneSettings", menuName = "ScriptableObjects/Plane Settings", order = 1)]
    public class PlaneSO : ScriptableObject
    {
        public string Type;
        public string Brand;
        public float MoveSpeed = 1f;
    }
}
